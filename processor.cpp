#include "processor.h"
#include <string>
Processor::Processor(QObject *parent) : QObject(parent)
{
    checkBox = false;
    status = 0;
    iter = 1;
}
void Processor::setInput_dir(string s){
    this->input_dir = s;
}
void Processor::doIteration(){
    if(stc.empty()){
        emit closeProgram();
        return;
    }
        thisFile = stc.pop();
        imgSaved=imread(thisFile.toStdString(), CV_LOAD_IMAGE_UNCHANGED);
        this->filename = thisFile.toStdString();
        if (imgSaved.empty())
        {
            emit doIteration();
        }
        else
        {
            emit sendImgName(thisFile);
            emit waitForKey(100);
        }
    }

void Processor::scanDir(QString dirName){
    thisDirectory = dirName;
    if(status == 1){
        return;
    }
    setInput_dir(dirName.toStdString());
    for(bfs::recursive_directory_iterator rdib(input_dir), rdie; rdib != rdie; ++rdib)
    {
        stc.push(QString::fromStdString(input_dir+rdib->path().filename().string()));
    }
    bfs::path* dir;
    string dir_direct_inner;
    string intToString_inner;
    if((bfs::is_directory(bfs::path(dirName.toStdString()  + "saves" + "/")))&&(this->checkBox)){
        bfs::remove_all(dirName.toStdString()  + "saves" + "/");
    }
    dir = new bfs::path(dir_direct_inner = dirName.toStdString()  + "saves");
    bfs::create_directory(*dir);
    delete dir;
    for(int i = 1; i < 10; i++){
        dir_direct_inner = dirName.toStdString()  + "saves" + "/";
        intToString_inner = to_string(i);
        dir = new bfs::path(dir_direct_inner+=intToString_inner);
        //qDebug() << QString::fromStdString(dir_direct_inner);
        bfs::create_directory(*dir);
        delete dir;
    }
    status = 1;
    emit doIteration();
}
void Processor::waitForKey(int butn){
    if((butn != 100) && (status == 1)){
        intToString = to_string(butn);
        intToString1 = to_string(iter);
        dir_name = thisDirectory.toStdString() + "saves/" + intToString + "/" + intToString1 +".jpg";
        iter = iter + 1;
       // qDebug() << QString::fromStdString(dir_name);
        imwrite(dir_name,imgSaved);
        emit doIteration();
    }
}
void Processor::changeCheckBoxStatus(bool checked){
    this->checkBox = checked;
}
void Processor::updateStatus(){
    this->status = 0;
    this->iter = 1;
    emit closeDialog();
}

