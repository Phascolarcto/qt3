#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <QObject>
#include <QDebug>
#include <QCoreApplication>
#include <QtWidgets>
#include <iostream>
#include <string>
#include <core/core.hpp>
#include <highgui/highgui.hpp>
#include <boost/program_options.hpp>
#include <features2d/features2d.hpp>
#include <cvaux.h>
#include <cv.h>
#include <direct.h>
#include <vector>
#include <algorithm>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/log/trivial.hpp>
#include <clocale>
#include <boost/log/utility/setup.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <ctime>
#include <QStack>
using namespace cv;
using namespace std;
using namespace boost::program_options;
namespace bfs = boost::filesystem;
namespace logging = boost::log;
namespace keywords = boost::log::keywords;
namespace src = boost::log::sources;
class Processor : public QObject
{
    Q_OBJECT
public:
    explicit Processor(QObject *parent = nullptr);
    void setInput_dir(string s);
    void set_rdib(string s);
    //void keyPressEvent(QKeyEvent* event);
  //  bfs::directory_iterator *rdib;
  // bfs::directory_iterator *rdie;
    Mat imgSaved;
    string intToString;
    string intToString1;
    string dir_direct;
    string dir_name;
    string input_dir;
    string filename;
    bfs::path* dir;
    QStack<QString> stc;
    QString thisFile;
    int status;
    QString thisDirectory;
    int iter;
    bool checkBox;
signals:
    void closeProgram();
    void sendImgName(QString);
    void closeDialog();
public slots:
   // void createDir(QString dirName);
    void doIteration();
    void waitForKey(int butn);
    void scanDir(QString dirName);
    void changeCheckBoxStatus(bool checked);
    void updateStatus();
};

#endif // PROCESSOR_H
