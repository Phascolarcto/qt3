#include "dialog.h"
#include <QDebug>
#include <QCoreApplication>
#include <QtWidgets>
#include "processor.h"
#include <QApplication>
#include "dialog3.h"
using namespace cv;
using namespace std;
using namespace boost::program_options;
namespace bfs = boost::filesystem;
namespace logging = boost::log;
namespace keywords = boost::log::keywords;
namespace src = boost::log::sources;
bool checkInputDir (string input_dir)
{
    try
    {
        boost::filesystem::recursive_directory_iterator rdib(input_dir);
    }
    catch (...)
    {
        BOOST_LOG_TRIVIAL(error)<<"ERROR! Incorrect input directory adress!\n";
        return false;
    }
    return true;
}
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Dialog w;
    Dialog3 dlg;
    Processor prcs;
    QObject::connect(&w,SIGNAL(butnpressed(int)),&prcs, SLOT(waitForKey(int)));
    QObject::connect(&w, SIGNAL(sendDirName(QString)),&prcs,SLOT(scanDir(QString)));
    QObject::connect(&prcs, SIGNAL(sendImgName(QString)),&w, SLOT(showImg(QString)));
    QObject::connect(&dlg, SIGNAL(end()),&a, SLOT(quit()));
    QObject::connect(&w,SIGNAL(sendCheckBoxStatus(bool)),&prcs, SLOT(changeCheckBoxStatus(bool)));
    QObject::connect(&prcs, SIGNAL(closeProgram()),&dlg, SLOT(show()));
    QObject::connect(&dlg, SIGNAL(start()),&prcs,SLOT(updateStatus()));
    QObject::connect(&prcs, SIGNAL(closeDialog()),&dlg,SLOT(close()));
    /*
    string intToString;
    string dir_direct;
   // string dir_name;
    bfs::path* dir;
    for(int i = 1; i < 10; i++){
        dir_direct = "C:/save/";
        intToString = to_string(i);
        dir = new bfs::path(dir_direct+=intToString);
        bfs::create_directory(*dir);
        delete dir;
    }
    */
    w.show();
    prcs.doIteration();
    return a.exec();
}
