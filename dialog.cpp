#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}
void Dialog::keyPressEvent(QKeyEvent *event){
    if((event->key()) > 48 && (event->key()<58)){
        emit butnpressed(event->key() - 48);
    }
}
void Dialog::on_pushButton_clicked()
{
    QString dirName = QFileDialog::getExistingDirectory(this, "Choose dir",QDir::homePath());
    dirName = dirName + "/";
    emit sendDirName(dirName);
}
void Dialog::showImg(QString imgName){
    QPixmap* qp = new QPixmap(imgName); //???
    QGraphicsScene* scene = new QGraphicsScene();
    if(ui->graphicsView->size().width() > ui->graphicsView->size().height()){
        scene->addPixmap(qp->scaledToHeight(ui->graphicsView->size().height()));
    }else{
        scene->addPixmap(qp->scaledToWidth(ui->graphicsView->size().width()));
    }
    ui->graphicsView->setScene(scene);
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->show();
    //QCoreApplication::processEvents();
}

void Dialog::on_checkBox_toggled(bool checked)
{
    emit sendCheckBoxStatus(checked);
}
