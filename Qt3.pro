#-------------------------------------------------
#
# Project created by QtCreator 2017-09-16T20:13:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Qt3
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

OPENCVDIR3 = "C:/Libs/opencv_320"

CONFIG( release, debug | release ){
    LIBS += -L$${OPENCVDIR3}/build/x64/vc14/lib/ \
                -lopencv_world320

}

CONFIG( debug, debug | release ){
    LIBS += -L$${OPENCVDIR3}/build/x64/vc14/lib/ \
                -lopencv_world320d
}

INCLUDEPATH += $${OPENCVDIR3}/build/include\
               $${OPENCVDIR3}/3rdparty/include\
               $${OPENCVDIR3}/build/include/opencv2\
               $${OPENCVDIR3}/build/include/opencv\

INCLUDEPATH += C:/Libs/boost_1_63_0
LIBS += -L"C:/Libs/boost_1_63_0/stage/lib"

DEPENDPATH += $${OPENCVDIR3}/build/x64/vc14/bin\

SOURCES += \
        main.cpp \
        dialog.cpp \
    processor.cpp \
    dialog3.cpp

HEADERS += \
        dialog.h \
    processor.h \
    dialog3.h

FORMS += \
        dialog.ui \
    dialog3.ui
